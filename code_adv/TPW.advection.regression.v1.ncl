; uses multiple linear regression to determin the weighting profile
; needed to estimate the true TPW advection using only the TPW
; and the vertically resolved winds.
; This stems from the advection scheme in the MIMIC TPW product.
; Satellite retrieval give a good estimate of TPW, and GFS
; gives a good estimate of the vertically resolved wind.
; Calculating TPW advection from that information is currently
; using an ad hoc method, which heavily weights near surface values.
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_ECMWF.ncl"
load "$NCARG_ROOT/custom_functions.ncl"
begin

	fig_type = "png"
	fig_file = "~/Research/DYNAMO/ECMWF/figs_adv/TPW.advection.regression.v1"
	
	debug   = False
	recalc  = False

	if debug then
		lat1 = -2.
		lat2 =  2.
		lon1 =  70.
		lon2 =  75.
	else
		lat1 = -20.
		lat2 =  20.
		lon1 =  50.
		lon2 = 100.
	end if
;====================================================================================================
;====================================================================================================
	wks = gsn_open_wks(fig_type,fig_file)
	gsn_define_colormap(wks,"WhiteBlueGreenYellowRed")
	;gsn_define_colormap(wks,"ncl_default")
	plot = new(1,graphic)
	num_p = dimsizes(plot)
		res = True
		res@gsnDraw                         = False
		res@gsnFrame                        = False
		res@tmXTOn                          = False
		res@tmYLMinorOn                     = False
		res@tmXBMinorOn                     = False
		res@gsnLeftStringFontHeightF        = 0.015
		res@gsnRightStringFontHeightF       = 0.015
		res@tmXBLabelFontHeightF            = 0.01
		res@tmYLLabelFontHeightF            = 0.01
		res@tmXBMajorOutwardLengthF         = 0.0
		res@tmXBMinorOutwardLengthF         = 0.0
		res@tmYLMajorOutwardLengthF         = 0.0
		res@tmYLMinorOutwardLengthF         = 0.0
		res@gsnLeftString                   = ""
		res@gsnRightString                  = ""
		
		lres = res
		lres@xyLineColor 		= "black"
		lres@xyDashPattern 		= 0
		lres@xyLineThicknessF	= 1.

		res@xyLineColor 		= "black"
		res@xyDashPattern 		= 0
		res@xyLineThicknessF	= 3.
;====================================================================================================
; Calculate advection components
;====================================================================================================
	if debug then t2 = debug_time_limit-1 else t2 = 368-1 end if
	setfileoption("nc","Format","NetCDF4")	
	opt = True
	opt@debug = debug
	opt@lat1 = lat1
	opt@lat2 = lat2
	opt@lon1 = lon1
	opt@lon2 = lon2
	;lev = (/70.,100.,150.,200.,250.,300.,400.,500.,600.,700.,800.,850.,900.,925.,950.,1000./)
	lev = (/500.,700.,850.,1000./)
	opt@lev = lev
	lat = LoadEClat(opt)
	lon = LoadEClon(opt)  
	levsz  = dimsizes(lev)
	latsz  = dimsizes(lat)
	lonsz  = dimsizes(lon)
	
;U  = LoadEC("U"    ,opt)
;	do t = 0,dimsizes(U(:,0,0,0))-1 print("t = "+t+"	# missing: "+num(ismissing(U(t,:,:,:)))) end do
;	do k = 0,dimsizes(U(0,:,0,0))-1 print("k = "+k+"	# missing: "+num(ismissing(U(:,k,:,:)))) end do
;exit
	
	;tfile = "~/Research/DYNAMO/ECMWF/data/EC.budget.TPW.advection.corr.v1.nc"
	tfile = "/data2/whannah/DYNAMO/ECMWF/data/EC.budget.TPW.advection.corr.v1.nc"
	print("  Loading True TPW Advection...")
	infile = addfile(tfile,"r")
	viUdQdx 	= infile->viUdQdx(:t2,{lat1:lat2},{lon1:lon2})
	viVdQdy 	= infile->viVdQdy(:t2,{lat1:lat2},{lon1:lon2})
	viVdelQ = viUdQdx + viVdQdy
	copy_VarCoords(viUdQdx,viVdelQ)
	;UdQdx    	= infile->UdQdx
	;VdQdy   	= infile->VdQdy
	;VdelQ 	= UdQdx   + VdQdy
	;copy_VarCoords(UdQdx,VdelQ)
;====================================================================================================
; Load TPW
;====================================================================================================
	print("  Loading TPW...")
	;ifile = "~/Research/DYNAMO/ECMWF/data/EC.budget.LQ.nc"
	ifile = "/data2/whannah/DYNAMO/ECMWF/data/EC.budget.LQ.nc"
	infile = addfile(ifile,"r")		
	TPW = infile->LQvi(:t2,:,:)
	TPW = (/TPW/Lv/)
	TPW@long_name = "TPW [mm]"
	print("done.")
	
	;delTPW = (/ calc_ddx(TPW) , calc_ddy(TPW) /)
	dduTPW = calc_ddx(TPW)
	ddyTPW = calc_ddy(TPW)
	delTPW = (/ ndtooned( dduTPW(:,{lat1:lat2},{lon1:lon2}) ) , ndtooned( ddyTPW(:,{lat1:lat2},{lon1:lon2}) ) /)
	
	print("  Loading U...")
	U  = LoadEC("U"    ,opt)
	print("  Loading V...")
	V  = LoadEC("V"    ,opt)
	print("done.")
	
	if True then
		print("==================================================================================")
		print("==================================================================================")
		printVarSummary(TPW)
		print("----------------------------------------------------------------------------------")
		printVarSummary(viVdelQ)
		print("----------------------------------------------------------------------------------")
		printVarSummary(U)
		print("==================================================================================")
		print("==================================================================================")
		if any(ismissing(TPW   )) then print("TPW    : "+num(ismissing(TPW   ))+" values missing!") else print("TPW    : no values missing") end if
		if any(ismissing(delTPW)) then print("delTPW : "+num(ismissing(delTPW))+" values missing!") else print("delTPW : no values missing") end if
		if any(ismissing(U     )) then print("U      : "+num(ismissing(U     ))+" values missing!") else print("U      : no values missing") end if
		if any(ismissing(V     )) then print("V      : "+num(ismissing(V     ))+" values missing!") else print("V      : no values missing") end if
		print("==================================================================================")
		print("==================================================================================")
	end if
;====================================================================================================
; Set Missing Values to 0.0
;====================================================================================================	
	do k = 0,levsz-1
		viVdelQ = where(ismissing(U(:,k,:,:)),0.0,viVdelQ)
		viVdelQ = where(ismissing(V(:,k,:,:)),0.0,viVdelQ)
	end do
	U = where(ismissing(U),0.0,U)
	V = where(ismissing(V),0.0,V)
	
if any(ismissing(U     )) then print("U      : "+num(ismissing(U     ))+" values missing!") else print("U      : no values missing") end if
if any(ismissing(V     )) then print("V      : "+num(ismissing(V     ))+" values missing!") else print("V      : no values missing") end if
;====================================================================================================
; Calculate Regression Vector    Y = X#a + e    Y ~ Nx1, X ~ NxM, N = npool, M = levsz
;====================================================================================================
	ntime = dimsizes(TPW(:,0,0))
	npool = latsz*lonsz*ntime
	
	X = new((/npool,levsz/),float)	
	Y = new((/npool/),float)
	

print("!")
	do k = 0,levsz-1
		X(:,k) = (/ delTPW(0,:) * ndtooned( U(:,k,:,:) ) + delTPW(1,:) * ndtooned( V(:,k,:,:) ) /)
	end do
print("!")
	
	Y = ndtooned( viVdelQ )
	
	;X = where(ismissing(X),0.0,X)
	;print("start loop")
	;do n = 0,npool-1  if any(ismissing(X(n,:))) then Y(n) = 0.0 end if end do
	;print("end loop")
	
	XT = transpose(X)
	
printVarSummary(X)
printVarSummary(XT)
printVarSummary(Y)
	
	a = inverse_matrix( XT # X ) # (XT # Y )
	
	print(a)
	
;====================================================================================================
; Create Plot
;====================================================================================================
	print("Creating plot...")
	
	tres = res
	tres@trYReverse	= True
	;tres@trXMinF	= -0.5
	;tres@trXMaxF	=  1.

	;--------------------------------------------------------------------------------
	tres@gsnLeftString = "a"
	plot(0) = gsn_csm_xy(wks,a,lev,tres)
	;----------------------------------------------
	;--------------------------------------------------------------------------------
	delete(tres)
	
	
	xx = (/-1e8,1e8/)
	do p = 0,dimsizes(plot)-1
	if .not.ismissing(plot(p)) then
		overlay(plot(p),gsn_csm_xy(wks,xx*0.,xx,lres))
	end if
	end do
;====================================================================================================
; Finalize Figure
;====================================================================================================
		pres = True
		;pres@txString                          = "Lagrangian TPW Tendency (shaded) and TPW (contours)"
		pres@gsnPanelBottom                     = 0.1 
		pres@amJust                             = "TopLeft"
		pres@gsnPanelFigureStringsFontHeightF   = 0.015
		;pres@gsnPanelFigureStrings              = (/"a","b","c","d"/) 
		;pres@gsnPanelYWhiteSpacePercent         = 5

	gsn_panel(wks,plot,(/num_p,1/),pres)
  
	print("")
	print(" "+fig_file+"."+fig_type)
	print("")
	if fig_type.eq."png" then system("convert -trim "+fig_file+"."+fig_type+"   "+fig_file+"."+fig_type) end if

;====================================================================================================
;====================================================================================================
end
