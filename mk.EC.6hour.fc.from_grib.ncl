load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
begin
	idir = "/Volumes/ECMWF/deterministic_forecast/"
	odir = "~/Research/DYNAMO/ECMWF/data/"
	
	;yrmn = (/201110,201111,201112,201201/)	
	yrmn = (/201110,201111,201112/)
	;yrmn = (/201110/)
	days_per_month = (/31,30,31/)
	
	lat1 = -40.
	lat2 =  40.
	
	lon1 =   0.
	lon2 = 360.
;=====================================================================================
; Switches & Constants
;=====================================================================================
	ilon1 = lon1+360.
	ilon2 = lon2+360.
	
	reorder_lon	= False

	; TSR_GDS0_SFC		Top Solar
	; TTR_GDS0_SFC		Top Thermal
	; SSR_GDS0_SFC		Sfc Solar
	; STR_GDS0_SFC		Sfc Thermal
	; SLHF_GDS0_SFC		Sfc Latent Heat Flux
	; SSHF_GDS0_SFC		Sfc Sensible Heat Flux

	ivar		= (/"TSR" ,"TTR" ,"SSR" ,"STR" ,"SLHF","SSHF"/)+"_GDS0_SFC"
	ovar		= (/"TNSW","TNLW","SNSW","SNLW", "LHF","SHF"/)

	;ivar = "SLHF_GDS0_SFC"
	;ovar = "LHF"
	
	num_v = dimsizes(ivar)
	num_m = dimsizes(yrmn)
;=====================================================================================
; Load data from Grib files
;=====================================================================================
do x = 0,0

	hour = (/"00","12"/)
	num_h = dimsizes(hour)	
	
do v = 0,num_v-1
	print("")
	print("	var 	"+ovar(v)+"	( "+ivar(v)+" )")
	print("")
	ofile = odir+"12hour.00-12.DYNAMO.ECMWF."+ovar(v)+""+".nc"

	do m = 0,num_m-1
		if m.eq.0 then t1 = 0   end if
		if m.ne.0 then t1 = sum(days_per_month(:m-1))*num_h   end if
	do d  = 0,days_per_month(m)-1
	do hr = 0,num_h-1
		;***********************************************
		; Define Forecast hour file name
		;***********************************************
		if hour(hr).eq."00" then 
			sm = sprinti("%2.2i",(yrmn(m)-201100))
			sd = sprinti("%2.2i",d+1-1)
			sh = "00"
			fm = sm
			fd = sprinti("%2.2i",d+1)
			fh = "00" 
		end if
		if hour(hr).eq."12" then 
			sm = sprinti("%2.2i",(yrmn(m)-201100))
			sd = sprinti("%2.2i",d+1)
			sh = "00"
			fm = sm
			fd = sprinti("%2.2i",d+1)
			fh = "12"
		end if
		if (d.eq.0).and.(hr.eq.0) then 
		if yrmn(m).eq.201110 then
			sd = "01"
			fname = "D1D"+sm+fd+sh+"00"+fm+fd+"12001" 	; First file of Oct
		else
			sm = sprinti("%2.2i",(yrmn(m)-201100)-1)
			sd = days_per_month(m-1)-1
			fname = "D1D"+sm+sd+sh+"00"+fm+fd+fh+"001" 	; First file (not Oct)
		end if
		else
			fname = "D1D"+sm+sd+sh+"00"+fm+fd+fh+"001" 
		end if
		ddir  = "2011"+sm+sd+"/"
		ifile = idir+ddir+fname+".grb"
		print("  converting "+ifile+"  >  "+ofile)
		grib_in	= addfile(ifile,"r")   
		names_in	= getfilevarnames(grib_in); extract all variable names 
		num_names 	= dimsizes(names_in)
		;***********************************************
		; Find lat and lon coord variable names
		;***********************************************
		latchk = new(num_names,logical)
		lonchk = new(num_names,logical)
		levchk = new(num_names,logical)
		do n = 0,num_names-1
		 if isStrSubset(names_in(n),"lat") then latchk(n) = True end if
		 if isStrSubset(names_in(n),"lon") then lonchk(n) = True end if
		 if isStrSubset(names_in(n),"lv_") then levchk(n) = True end if
		 if isStrSubset(names_in(n),"lv_") then lev_name = names_in(ind(levchk)) end if
		end do
		lat_name = names_in(ind(latchk))
		lon_name = names_in(ind(lonchk))
		num_dims = 2;dimsizes(dimsizes(grib_in->$ivar(v)$))
	;***********************************************
	; Define Vout
	;***********************************************
	if (m.eq.0).and.(d.eq.0).and.(hr.eq.0) then
		tsz 	   	 = sum(days_per_month)*num_h
		time    	 = fspan(0,tsz-1,tsz)*12.
		time!0  	 = "time"
		time@units 	 = "Hours since Oct. 1, 2011"
		if num_dims.eq.3 then
			lev       	 = grib_in->$lev_name$({70:1000})
			lev!0     	 = "lev"
			lev@units 	 = "mb"
			levsz     	 = dimsizes(lev)
		end if    
		; Latitude
		lat 			= grib_in->$lat_name$({lat1:lat2:-1})
		lat!0	 		= "lat"
		lat@long_name  = "Latitude"
		lat@units		= "degrees_north"
		latsz 	 		= dimsizes(lat)
		; Longitude
		tlon        	= grib_in->$lon_name$({ilon1:ilon2})
		lon	         	= (/tlon-360./)
		lon!0	 		= "lon"
		lon@long_name  = "Longitude"
		lon@units		= "degrees_east"
		lonsz 	 		= dimsizes(lon) 
		if num_dims.eq.3 then
			Vout = new((/tsz,levsz,latsz,lonsz/),float)
		else
			Vout = new((/tsz,latsz,lonsz/),float)
		end if
		Vout = (/0./)
	end if
	;***********************************************
	;***********************************************
		tt = t1+d*num_h+hr
		if (m.eq.0).and.(d.eq.0).and.(hr.eq.0) then 
			Vout(tt,:,:) = Vout@_FillValue
		else
			Vout(tt,:,:) = grib_in->$ivar(v)$({lat1:lat2:-1},{ilon1:ilon2})
		end if
		delete([/names_in,latchk,lonchk,levchk/])
		;***********************************************
		;***********************************************
	end do   ; h = 0,1
	end do   ; d = 0,days_per_month(m)-1
	end do   ; m = 0,num_m-1
	;================================================================
	; fix units of accumulated fields
	;================================================================
	tmp = Vout
	Vout(0::2,:,:) = ( tmp(0::2,:,:)  			      	)/(12.*3600.)
	Vout(1::2,:,:) = ( tmp(1::2,:,:) - tmp(0::2,:,:)	)/(12.*3600.)
	Vout@units = "W m**-2"
	delete(tmp)
	;================================================================
	;================================================================
		
		if num_dims.eq.3 then
			Vout!0 = "time"
			Vout!1 = "lev"
			Vout!2 = "lat"
			Vout!3 = "lon"
			delete(Vout&lev)
			Vout&lev = lev
		end if
		if num_dims.eq.2 then
			Vout!0 = "time"
			Vout!1 = "lat"
			Vout!2 = "lon"
		end if

		delete(Vout&lat)
		delete(Vout&lon)
		
		Vout&lat = lat
		Vout&lon = lon
		
		if ivar(v).eq."PV" then
			Vout@level = "315 K"
		end if

	;***********************************************
	; Reorder the longitude if necessary.
	;***********************************************
	if(reorder_lon)then
		print("    Reordering Longitude...")
		; reorder so that lon -> 0:358.5 instead of -180:178.5
		ilon = lon
		delete(lon)
		lon = ilon + 180
		j = ispan(0,latsz-1,1)
			if num_dims.eq.3 then
				tmp = Vout(:,:,j,:)
				Vout(:,:,j,       :lonsz/2-1) = tmp(:,:,j,lonsz/2:         )
				Vout(:,:,j,lonsz/2:         ) = tmp(:,:,j,       :lonsz/2-1)
			else
				tmp = Vout;(:,j,:)
				Vout(:,j,       :lonsz/2-1) = tmp(:,j,lonsz/2:         )
				Vout(:,j,lonsz/2:         ) = tmp(:,j,       :lonsz/2-1)
			end if
			delete(tmp)
	 lon!0 = "lon"
	 lon&lon = lon
	 Vout&lon = lon
	end if
	;-----------------------------------------------
	; Write to file
	;-----------------------------------------------
	if isfilepresent(ofile) then
		system("rm "+ofile)
	end if
	outfile = addfile(ofile,"c")
	outfile->$ovar(v)$ = Vout
print(outfile)
	;if num_dims.eq.3 then outfile->lev = lev end if
	;outfile->lat = lat
	;outfile->lon = lon
	print("")
 
		delete([/Vout,lat,lon/])
		if isvar("lev") then
			delete(lev)
		end if
		
end do  ; v = 0,num_v-1
end do	; 0,1
;=====================================================================================
;=====================================================================================
end
