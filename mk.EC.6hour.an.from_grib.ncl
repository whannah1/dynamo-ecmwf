load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
begin
	;idir = "/media/DYNAMO/dynamo/model/ECMWF/deterministic_analysis/"
	;idir = "~/Research/DYNAMO/ECMWF/raw_data/"
	idir = "/data2/brian/"

	odir = "~/Research/DYNAMO/ECMWF/data/"
	
	;yrmn = (/201110,201111,201112,201201/)	
	yrmn = (/201110,201111,201112/)
	;yrmn = (/201110/)
	days_per_month = (/31,30,31/)
	
	lat1 = -40.
	lat2 =  40.
	
	lon1 =   0.
	lon2 = 360.
;=====================================================================================
; Switches & Constants
;=====================================================================================
  ilon1 = lon1+360.
  ilon2 = lon2+360.
  
  reorder_lon	= False

	; T_GDS0_ISBL	Temperaure
	; Z_GDS0_ISBL	Geopotential
	; Q_GDS0_ISBL	Specific Humidity
	; U_GDS0_ISBL	Zonal Wind
	; V_GDS0_ISBL	Meridional wind
	; W_GDS0_ISBL	Vertical pressure velocity
	; SP_GDS0_SFC	Surface Pressure

  ;ivar		= (/"T_GDS0_ISBL","Q_GDS0_ISBL","Z_GDS0_ISBL","W_GDS0_ISBL","U_GDS0_ISBL","V_GDS0_ISBL"/)
  ;ovar		= (/"T","Q","GEO","OMEGA","U","V"/)

  ;ivar		= (/"T_GDS0_ISBL","Q_GDS0_ISBL","Z_GDS0_ISBL","W_GDS0_ISBL"/)
  ;ovar		= (/"T","Q","GEO","OMEGA"/)
  
  ;ivar = (/"SP_GDS0_SFC"/)
  ;ovar = (/"Ps"/)
  
  ivar = (/"CWV_GDS0_SFC"/)
  ovar = (/"CWV"/)
  
  ;ivar		= (/"U_GDS0_ISBL","V_GDS0_ISBL","Q_GDS0_ISBL"/)
  ;ovar   	= (/"U","V","Q"/)
  
  ;hour 	= (/"00","06","12","18"/)

  num_v = dimsizes(ivar)
  num_m = dimsizes(yrmn)
;=====================================================================================
; Load data from Grib files
;=====================================================================================
do x = 0,1
 
  if x.eq.0 then hour = (/"00","12"/) end if
  if x.eq.1 then hour = (/"06","18"/) end if
  num_h = dimsizes(hour)	
  
do v = 0,num_v-1
	print("")
	print("	var 	"+ovar(v)+"	( "+ivar(v)+" )")
	print("")
  ;ofile = odir+"6hour.DYNAMO.ECMWF."     +ovar(v)+""+".nc"
  if all(hour.eq.(/"00","12"/)) then ofile = odir+"6hour.00-12.DYNAMO.ECMWF."+ovar(v)+""+".nc" end if
  if all(hour.eq.(/"06","18"/)) then ofile = odir+"6hour.06-18.DYNAMO.ECMWF."+ovar(v)+""+".nc" end if

  do m = 0,num_m-1
    if m.eq.0 then t1 = 0   end if
    if m.ne.0 then t1 = sum(days_per_month(:m-1))*num_h   end if
  do d = 0,days_per_month(m)-1
  do hr = 0,num_h-1
    ;***********************************************
    ; Get variable names from grib file
    ;***********************************************
    sm = sprinti("%2.2i",(yrmn(m)-201100))
    sd = sprinti("%2.2i",d+1)
    sh = hour(hr)
    ddir  = "";yrmn(m)+sd+"/"
    fname = "D1D"+sm+sd+sh+"00"+sm+sd+sh+"001.grb1"
    ifile = idir+ddir+fname
    
    print("  converting "+ifile+"  >  "+ofile)
    grib_in	= addfile(ifile,"r")   

    names_in	= getfilevarnames(grib_in); extract all variable names 
    num_names 	= dimsizes(names_in)
    
    ; Find lat and lon coord variable names
    latchk = new(num_names,logical)
    lonchk = new(num_names,logical)
    levchk = new(num_names,logical)
    do n = 0,num_names-1
    if isStrSubset(names_in(n),"lat") then
      latchk(n) = True
     end if
     if isStrSubset(names_in(n),"lon") then
       lonchk(n) = True
     end if
     if isStrSubset(names_in(n),"lv_") then
      levchk(n) = True
      lev_name = names_in(ind(levchk))
     end if
    end do
    lat_name = names_in(ind(latchk))
    lon_name = names_in(ind(lonchk))
    
   num_dims	= dimsizes(dimsizes(grib_in->$ivar(v)$))

	;----------------------------------------------------
	; Define Vout
	;----------------------------------------------------
	if (m.eq.0).and.(d.eq.0).and.(hr.eq.0) then
	 tsz 	   	 = sum(days_per_month)*num_h
   	 time    	 = fspan(0,tsz-1,tsz)*6.
   	 time!0  	 = "time"
   	 time@units 	 = "Hours since Oct. 1, 2011"
   	 if num_dims.eq.3 then
   	   lev       	 = grib_in->$lev_name$({70:1000})
   	   lev!0     	 = "lev"
   	   lev@units 	 = "mb"
   	   levsz     	 = dimsizes(lev)
   	 end if    
	 ; Latitude
	 lat 		= grib_in->$lat_name$({lat1:lat2:-1})
	 lat!0	 	= "lat"
	 lat@long_name  = "Latitude"
	 lat@units	= "degrees_north"
	 latsz 	 	= dimsizes(lat)
	 ; Longitude
	 tlon        	= grib_in->$lon_name$({ilon1:ilon2})
	 lon		= (/tlon-360./)
	 ;lon		= tlon({lon1:lon2})
	 lon!0	 	= "lon"
	 lon@long_name  = "Longitude"
	 lon@units	= "degrees_east"
	 lonsz 	 	= dimsizes(lon) 
	 if num_dims.eq.3 then
 	   Vout = new((/tsz,levsz,latsz,lonsz/),float)
 	 else
 	   Vout = new((/tsz,latsz,lonsz/),float)
 	 end if
 	 ;Vout = (/0./)
	end if
	;----------------------------------------------------
	;----------------------------------------------------
    tt = t1+d*num_h+hr
    
        ;wks = gsn_open_wks("x11","Test")
    	;  	res = True
    	;  	res@cnFillOn = True
    	;plot = gsn_csm_contour_map(wks,grib_in->$ivar(v)$({1000},::-1,:),res)
    	;exit
    
    if num_dims.eq.3 then Vout(tt,:,:,:) = grib_in->$ivar(v)$({70:1000},{lat1:lat2:-1},{ilon1:ilon2}) 	end if
    if num_dims.ne.3 then Vout(tt,:,:)   = grib_in->$ivar(v)$({lat1:lat2:-1},{ilon1:ilon2}) 		end if
      delete([/names_in,latchk,lonchk,levchk/])
  end do   ; h = 0,3
  end do   ; d = 0,days_per_month(m)-1
  end do   ; m = 0,num_m-1
  ;================================================================
  ;================================================================
    
    if num_dims.eq.3 then
      Vout!0 = "time"
      Vout!1 = "lev"
      Vout!2 = "lat"
      Vout!3 = "lon"
      delete(Vout&lev)
      Vout&lev = lev
    end if
    if num_dims.eq.2 then
      Vout!0 = "time"
      Vout!1 = "lat"
      Vout!2 = "lon"
    end if
    
printVarSummary(Vout)
;printVarSummary(Vout&lat)
;printVarSummary(lat)

    delete(Vout&lat)
    delete(Vout&lon)
    
    Vout&lat = lat
    Vout&lon = lon
    
    if ivar(v).eq."PV" then
      Vout@level = "315 K"
    end if
    

  ;***********************************************
  ; Reorder the longitude if necessary.
  ;***********************************************
  if(reorder_lon)then
    print("    Reordering Longitude...")
    ; reorder so that lon -> 0:358.5 instead of -180:178.5
    ilon = lon
    delete(lon)
    lon = ilon + 180
    j = ispan(0,latsz-1,1)
      if num_dims.eq.3 then
        tmp = Vout(:,:,j,:)
        Vout(:,:,j,       :lonsz/2-1) = tmp(:,:,j,lonsz/2:         )
        Vout(:,:,j,lonsz/2:         ) = tmp(:,:,j,       :lonsz/2-1)
      else
        tmp = Vout;(:,j,:)
        Vout(:,j,       :lonsz/2-1) = tmp(:,j,lonsz/2:         )
        Vout(:,j,lonsz/2:         ) = tmp(:,j,       :lonsz/2-1)
      end if
      delete(tmp)
   lon!0 = "lon"
   lon&lon = lon
   Vout&lon = lon
  end if
  ;-----------------------------------------------
  ; Write to file
  ;-----------------------------------------------
  if isfilepresent(ofile) then
    system("rm "+ofile)
  end if
  outfile = addfile(ofile,"c")
  outfile->$ovar(v)$ = Vout
  if num_dims.eq.3 then
    outfile->lev = lev
  end if
  outfile->lat = lat
  outfile->lon = lon
  print("")
 
    delete([/Vout,lat,lon/])
    if isvar("lev") then
      delete(lev)
    end if
    
end do  ; v = 0,num_v-1
end do	; 0,1
;=====================================================================================
;=====================================================================================
end
