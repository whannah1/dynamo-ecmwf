                                      ECMWF Model Output for DYNAMO


This external hard drive contains four ECMWF model data sets for the DYNAMO project that occurred from 
1 October 2011 through 31 March 2012 in the Indian Ocean.  In total the four data sets included on this drive
comprise 2.18Tb.


    1) ECMWF Ensemble Prediction System (EPS) output

    This data set is located in the dynamo/model/ECMWF/eps directory.  For each day there is a 15-day forecast 
    initialized at 00 UTC.  There is one file for each day of the forecast (15 files per model run).  The output 
    is at 1-degree resolution over the DYNAMO domain which is defined as 30S-30N and 30-150E.  Each day is ~0.55Gb 
    for a data set total of 108Gb.  These data are in GRIB1 format.

    2) ECMWF Deterministic Model Analysis Data on Pressure Levels in GRIB1

    This data set is located in the dynamo/model/ECMWF/deterministic_analysis directory and consists of the files 
    ending in .grb1.  For each day there are analysis files at 00, 06, 12, and 18 UTC.  The output is at 0.25 degree 
    resolution and has global coverage.  Each day is ~1.7Gb for a data set total of ~311Gb.  These data are in GRIB1 
    format.

    3) ECMWF Deterministic Model Analysis Data on Hybrid Levels in GRIB2

    This data set is located in the dynamo/model/ECMWF/deterministic_analysis directory and consists of the files 
    ending in .grb2.  For each day there are analysis files at 00, 06, 12, and 18 UTC.  The output is at 0.25 degree 
    resolution and has global coverage.  Each day is ~6.1Gb for a data set total of ~1.1Tb.  These data are in GRIB2 
    format.

    4) ECMWF Deterministic Model Forecast Data

    This data set is located in the dynamo/model/ECMWF/deterministic_forecast directory.  For each day there there is 
    a 10-day forecast initialized at 00 UTC.  There is one file forevery 12 hours of the forecast (20 files per model 
    run).  The output is at 0.25 degree resolution and has global coverage.  Each day is ~4.8Gb for a data set total 
    of 815Gb.  These data are in GRIB1 format.


File Naming Convention


    All data sets use the same naming convention.

    cccMMDDHHIImmddhhiiE where:

    ccc  is    Dissemination stream name (D1D for deterministic and E1E for ensemble)

    MMDDHHII is month, day, hour and minute on which the products are based;

    mmddhhii ismonth, day, hour and minute on which the products are valid at;

    E is "1" for all current operational products.

    For example, D1D10030000100312001 is from the deterministic model run initialized 
    at 0000 UTC on 3 October and it is for the forecast valid at 1200 UTC 3 October.


Documentation


    ECMWF has some documentation on-line for its modeling system:

    http://www.ecmwf.int/services/dissemination/3.1/

    http://www.ecmwf.int/research/

    http://www.ecmwf.int/research/ifsdocs/


License Agreement


    For usage restrictions of this data set please read the ECMWF_License_Agreement.txt file included on this disk.
    This is the agreement that ECMWF and DYNAMO reached on the usage of these data.


Contact


    If you have any problems with the data contained on this drive please contact:

    Steve Williams

    sfw@ucar.edu
