load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/custom_functions_DYNAMO_ECMWF.ncl"
begin

	fig_type = "pdf"
	fig_file = "~/Research/DYNAMO/ECMWF/figs_budget/LQ.lagrangian.map.v2"

	lat1 = -15.
	lat2 =  15.
	lon1 =  40.
	lon2 = 180.

	DrawBox = False		; Draw DYNAMO sounding array
	addvc   = True 		; Add wind vectors

	;t1 = (31+25)*4
	;t2 = t1+4

	;t1 = (00)*4
	;t2 = (31)*4

;====================================================================================================
;====================================================================================================
	wks = gsn_open_wks(fig_type,fig_file)
	gsn_define_colormap(wks,"WhiteBlueGreenYellowRed")
	;gsn_define_colormap(wks,"ncl_default")
	plot = new(2,graphic)
	num_p = dimsizes(plot)
		res = True
		res@gsnDraw                         = False
		res@gsnFrame                        = False
		res@tmXTOn                          = False
		res@tmYLMinorOn                     = False
		res@tmXBMinorOn                     = False
		res@gsnAddCyclic                    = False
		res@gsnLeftStringFontHeightF        = 0.015
		res@gsnRightStringFontHeightF       = 0.015
		res@lbTitleFontHeightF              = 0.012
		res@lbLabelFontHeightF              = 0.012
		res@tmXBLabelFontHeightF            = 0.01
		res@tmYLLabelFontHeightF            = 0.01
		res@tmXBMajorOutwardLengthF         = 0.0
		res@tmXBMinorOutwardLengthF         = 0.0
		res@tmYLMajorOutwardLengthF         = 0.0
		res@tmYLMinorOutwardLengthF         = 0.0
		res@pmLabelBarOrthogonalPosF        = 0.12
		res@gsnLeftString                   = ""
		res@gsnRightString                  = ""
		res@cnLineLabelsOn                  = False
		res@cnInfoLabelOn                   = False

		;vres = res
		vres = True
		vres@gsnDraw                        = False
		vres@gsnFrame                       = False
		vres@vcRefMagnitudeF = 5.0                ; define vector ref mag
		vres@vcRefLengthF    = 0.01               ; define length of vec ref
		vres@vcGlyphStyle    = "CurlyVector"      ; turn on curley vectors
		vres@vcMinDistanceF  = 0.02               ; thin out vectors
;====================================================================================================
; Load Data
;====================================================================================================
	ifile = "~/Research/DYNAMO/ECMWF/data/EC.budget.LQ.nc"
	infile = addfile(ifile,"r")
	
	Q       = infile->LQvi
	DLQDT   = infile->DLQDT
	udQdx   = infile->udQdx
	vdQdy   = infile->vdQdy
	VdelQ   = infile->VdelQ

	TPW = Q
	TPW = (/Q/Lv/)
	TPW@long_name = "TPW [mm]"

	LT = ( DLQDT + udQdx + vdQdy )/Lv
	;LT = ( DLQDT + VdelQ )/Lv
	ADV = -( udQdx + vdQdy )/Lv
	;ADV = -( VdelQ )/Lv
	LT  = (/LT *86400/)
	ADV = (/ADV*86400/)
	DDT = (/DLQDT/Lv*86400/)
	copy_VarCoords(Q,LT)
	copy_VarCoords(Q,ADV)
	copy_VarCoords(Q,DDT)
	LT@long_name  = ""
	ADV@long_name = ""

	opt = True
	opt@lat1 = lat1
	opt@lat2 = lat2
	opt@lon1 = lon1
	opt@lon2 = lon2
	opt@lev  = 700.
	if addvc then U = LoadEC("U",opt) end if
	if addvc then V = LoadEC("V",opt) end if
;====================================================================================================
; Plot Map
;====================================================================================================
	if .not.isvar("t1") then t1 = 0 end if
	if .not.isvar("t2") then t2 = dimsizes(Q(:,0,0))-1 end if

		tres = res
		tres@cnFillPalette              = "BlueRed"
		;tres@cnFillPalette             = "BlRe"
		tres@mpLimitMode                = "LatLon"
		tres@mpMinLatF                  = lat1
		tres@mpMaxLatF                  = lat2
		tres@mpMinLonF                  = lon1
		tres@mpMaxLonF                  = lon2
		tres@cnFillOn                   = True
		tres@cnLinesOn                  = False
		;tres@gsnSpreadColors           = True
		tres@cnSpanFillPalette          = True
		tres@lbLabelBarOn               = True
		tres@cnLevelSelectionMode       = "ExplicitLevels"
		tres@lbTitlePosition            = "Bottom"
		tres@gsnLeftString              = ""

		;tres@cnLevels                   = ispan(-20,20,10)
		tres@cnLevels                   = ispan(-6,6,2)

		tres@lbTitleString              = "Lagrangian TPW Tendency [mm day~S~-1~N~]"
	plot(0) = gsn_csm_contour_map(wks,dim_avg_n_Wrap(LT(t1:t2,:,:),0),tres)

	;	tres@lbTitleString              = "TPW Tendency [mm day~S~-1~N~]"
	;plot(1) = gsn_csm_contour_map(wks,dim_avg_n_Wrap(DDT(t1:t2,:,:),0),tres)

		tres@lbTitleString              = "Advective TPW Tendency [mm day~S~-1~N~]"
	plot(1) = gsn_csm_contour_map(wks,dim_avg_n_Wrap(ADV(t1:t2,:,:),0),tres)
	delete(tres)

		tres = res
		tres@cnFillOn                   = False
		tres@cnLinesOn                  = True
		tres@cnLevelSelectionMode       = "ExplicitLevels"
		;tres@cnLevels                   = ispan(5,65,20)
		tres@cnLevels                   = ispan(20,80,5)
		tres@cnLineThicknessF           = 1.

	do p = 0,num_p-1 overlay(plot(p),gsn_csm_contour(wks,dim_avg_n_Wrap(TPW(t1:t2,:,:),0),tres)) end do
	delete(tres)

		tres = res
		tres@cnFillOn                   = False
		tres@cnLinesOn                  = True
		tres@cnLevelSelectionMode       = "ExplicitLevels"
		tres@cnLevels                   = ispan(50,50,1)
		tres@cnLineThicknessF           = 3.

	;do p = 0,num_p-1 overlay(plot(p),gsn_csm_contour(wks,dim_avg_n_Wrap(TPW(t1:t2,:,:),0),tres)) end do
	delete(tres)


	if addvc then 
		aU = dim_avg_n_Wrap(U(t1:t2,:,:),0)
		aV = dim_avg_n_Wrap(V(t1:t2,:,:),0)
		do p = 0,num_p-1 overlay(plot(p),gsn_csm_vector(wks,aU,aV,vres))  end do
		delete([/U,V,aU,aV/])
	end if

;====================================================================================================
; Add Box
;====================================================================================================
if DrawBox then 
		mres = True
		mres@gsMarkerIndex = 1
		mres@gsMarkerSizeF = 0.02
		mres@gsMarkerColor = "black"
	site_lat  = (/-0.7,     0.,     -7.3,   -7.3 ,   4.1,  6.9,  0./)
	site_lon  = (/73.2,     80.,        80.,        72.5 ,  73.5, 79.8, 80./)
	;--------------------------------------------------------------------------------------------
	; Add Shaded Polygon
	;--------------------------------------------------------------------------------------------
	;plat1 = -10.
	;plat2 =  10.
	;plon1 =  65.
	;plon2 =  85.
	plat1 =  -1.
	plat2 =   8.
	plon1 =  72.
	plon2 =  81.
	pgx = (/plon1,plon1,plon2,plon2,plon1/)
	pgy = (/plat1,plat2,plat2,plat1,plat1/)
			pgres                       = True
			pgres@gsFillColor           = "black"
			pgres@gsFillOpacityF        = 0.3
	do p = 0,num_p-1 gdum = gsn_add_polygon(wks,plot(p),pgx,pgy,pgres) end do
	;--------------------------------------------------------------------------------------------
	; Add Site Markers
	;--------------------------------------------------------------------------------------------
	num_s = dimsizes(site_lat)
	dum = new(num_s,graphic)
	do s = 0,num_s-1
	  do p = 0,num_p-1 dum(s) = gsn_add_polymarker(wks,plot(p),site_lon(s),site_lat(s),mres) end do
	end do
	;--------------------------------------------------------------------------------------------
	; Add Array lines
	;--------------------------------------------------------------------------------------------
		lres = True
		lres@gsLineColor = "black"
	; NSA+SSA
	array_lat = new(8,float)
	array_lon = new(8,float)
	array_lat(:3) = site_lat(:3)
	array_lon(:3) = site_lon(:3)
	array_lat(4)  = site_lat(0)
	array_lon(4)  = site_lon(0)
	array_lat(5:) = site_lat(4:)
	array_lon(5:) = site_lon(4:)
	do p = 0,num_p-1 ldum = gsn_add_polyline(wks,plot(p),array_lon,array_lat,lres) end do
	;--------------------------------------------------------------------------------------------
	;--------------------------------------------------------------------------------------------
end if
;====================================================================================================
; Finalize Figure
;====================================================================================================
		pres = True
		;pres@txString                           = "Lagrangian TPW Tendency (shaded) and TPW (contours)"
		;pres@gsnPanelBottom                    = 0.1 
		;pres@amJust                            = "TopLeft"
		;pres@gsnPanelFigureStringsFontHeightF  = 0.015
		;pres@gsnPanelFigureStrings             = (/"a","b","c","d"/) 

	gsn_panel(wks,plot,(/num_p,1/),pres)
  
	print("")
	print(" "+fig_file+"."+fig_type)
	print("")
	if fig_type.eq."png" then system("convert -trim "+fig_file+"."+fig_type+"   "+fig_file+"."+fig_type) end if

;====================================================================================================
;====================================================================================================
end