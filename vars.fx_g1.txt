 Copyright (C) 1995-2012 - All Rights Reserved
 University Corporation for Atmospheric Research
 NCAR Command Language Version 6.1.0-beta
 The use of this software is governed by a License Agreement.
 See http://www.ncl.ucar.edu/ for more details.

Variable: f
Type: file
filename:	D1D10010000100112001
path:	/media/DYNAMO/dynamo/model/ECMWF/deterministic_forecast/20111001/D1D10010000100112001
   file global attributes:
   dimensions:
      g0_lat_0 = 721
      g0_lon_1 = 1440
      lv_ISBL2 = 25
   variables:
      float SSTK_GDS0_SFC ( g0_lat_0, g0_lon_1 )
         center :	European Center for Medium-Range Weather Forecasts (RSMC)
         long_name :	Sea surface temperature
         units :	K
         _FillValue :	1e+20
         level_indicator :	1
         gds_grid_type :	0
         parameter_table_version :	128
         parameter_number :	34
         forecast_time :	12
         forecast_time_units :	hours
         initial_time :	10/01/2011 (00:00)

      float SWVL1_GDS0_DBLY ( g0_lat_0, g0_lon_1 )
         center :	European Center for Medium-Range Weather Forecasts (RSMC)
         long_name :	Volumetric soil water layer 1
         units :	m**3 m**-3
         _FillValue :	1e+20
         level_indicator :	112
         gds_grid_type :	0
         parameter_table_version :	128
         parameter_number :	39
         level :	( 0, 7 )
         forecast_time :	12
         forecast_time_units :	hours
         initial_time :	10/01/2011 (00:00)

      float CAPE_GDS0_SFC ( g0_lat_0, g0_lon_1 )
         center :	European Center for Medium-Range Weather Forecasts (RSMC)
         long_name :	Convective available potential energy
         units :	J kg**-1
         _FillValue :	1e+20
         level_indicator :	1
         gds_grid_type :	0
         parameter_table_version :	128
         parameter_number :	59
         forecast_time :	12
         forecast_time_units :	hours
         initial_time :	10/01/2011 (00:00)

      float Z_GDS0_ISBL ( lv_ISBL2, g0_lat_0, g0_lon_1 )
         center :	European Center for Medium-Range Weather Forecasts (RSMC)
         long_name :	Geopotential
         units :	m**2 s**-2
         _FillValue :	1e+20
         level_indicator :	100
         gds_grid_type :	0
         parameter_table_version :	128
         parameter_number :	129
         forecast_time :	12
         forecast_time_units :	hours
         initial_time :	10/01/2011 (00:00)

      float T_GDS0_ISBL ( lv_ISBL2, g0_lat_0, g0_lon_1 )
         center :	European Center for Medium-Range Weather Forecasts (RSMC)
         long_name :	Temperature
         units :	K
         _FillValue :	1e+20
         level_indicator :	100
         gds_grid_type :	0
         parameter_table_version :	128
         parameter_number :	130
         forecast_time :	12
         forecast_time_units :	hours
         initial_time :	10/01/2011 (00:00)

      float U_GDS0_ISBL ( lv_ISBL2, g0_lat_0, g0_lon_1 )
         center :	European Center for Medium-Range Weather Forecasts (RSMC)
         long_name :	U velocity
         units :	m s**-1
         _FillValue :	1e+20
         level_indicator :	100
         gds_grid_type :	0
         parameter_table_version :	128
         parameter_number :	131
         forecast_time :	12
         forecast_time_units :	hours
         initial_time :	10/01/2011 (00:00)

      float V_GDS0_ISBL ( lv_ISBL2, g0_lat_0, g0_lon_1 )
         center :	European Center for Medium-Range Weather Forecasts (RSMC)
         long_name :	V velocity
         units :	m s**-1
         _FillValue :	1e+20
         level_indicator :	100
         gds_grid_type :	0
         parameter_table_version :	128
         parameter_number :	132
         forecast_time :	12
         forecast_time_units :	hours
         initial_time :	10/01/2011 (00:00)

      float SP_GDS0_SFC ( g0_lat_0, g0_lon_1 )
         center :	European Center for Medium-Range Weather Forecasts (RSMC)
         long_name :	Surface pressure
         units :	Pa
         _FillValue :	1e+20
         level_indicator :	1
         gds_grid_type :	0
         parameter_table_version :	128
         parameter_number :	134
         forecast_time :	12
         forecast_time_units :	hours
         initial_time :	10/01/2011 (00:00)

      float TCWV_GDS0_SFC ( g0_lat_0, g0_lon_1 )
         center :	European Center for Medium-Range Weather Forecasts (RSMC)
         long_name :	Total column water vapour
         units :	kg m**-2
         _FillValue :	1e+20
         level_indicator :	1
         gds_grid_type :	0
         parameter_table_version :	128
         parameter_number :	137
         forecast_time :	12
         forecast_time_units :	hours
         initial_time :	10/01/2011 (00:00)

      float STL1_GDS0_DBLY ( g0_lat_0, g0_lon_1 )
         center :	European Center for Medium-Range Weather Forecasts (RSMC)
         long_name :	Soil temperature level 1
         units :	K
         _FillValue :	1e+20
         level_indicator :	112
         gds_grid_type :	0
         parameter_table_version :	128
         parameter_number :	139
         level :	( 0, 7 )
         forecast_time :	12
         forecast_time_units :	hours
         initial_time :	10/01/2011 (00:00)

      float LSP_GDS0_SFC ( g0_lat_0, g0_lon_1 )
         center :	European Center for Medium-Range Weather Forecasts (RSMC)
         long_name :	Stratiform precipitation (Large-scale precipitation)
         units :	m
         _FillValue :	1e+20
         level_indicator :	1
         gds_grid_type :	0
         parameter_table_version :	128
         parameter_number :	142
         forecast_time :	12
         forecast_time_units :	hours
         initial_time :	10/01/2011 (00:00)

      float CP_GDS0_SFC ( g0_lat_0, g0_lon_1 )
         center :	European Center for Medium-Range Weather Forecasts (RSMC)
         long_name :	Convective precipitation
         units :	m
         _FillValue :	1e+20
         level_indicator :	1
         gds_grid_type :	0
         parameter_table_version :	128
         parameter_number :	143
         forecast_time :	12
         forecast_time_units :	hours
         initial_time :	10/01/2011 (00:00)

      float SSHF_GDS0_SFC ( g0_lat_0, g0_lon_1 )
         center :	European Center for Medium-Range Weather Forecasts (RSMC)
         long_name :	Surface sensible heat flux
         units :	W m**-2 s
         _FillValue :	1e+20
         level_indicator :	1
         gds_grid_type :	0
         parameter_table_version :	128
         parameter_number :	146
         forecast_time :	12
         forecast_time_units :	hours
         initial_time :	10/01/2011 (00:00)

      float SLHF_GDS0_SFC ( g0_lat_0, g0_lon_1 )
         center :	European Center for Medium-Range Weather Forecasts (RSMC)
         long_name :	Surface latent heat flux
         units :	W m**-2 s
         _FillValue :	1e+20
         level_indicator :	1
         gds_grid_type :	0
         parameter_table_version :	128
         parameter_number :	147
         forecast_time :	12
         forecast_time_units :	hours
         initial_time :	10/01/2011 (00:00)

      float MSL_GDS0_SFC ( g0_lat_0, g0_lon_1 )
         center :	European Center for Medium-Range Weather Forecasts (RSMC)
         long_name :	Mean sea level pressure
         units :	Pa
         _FillValue :	1e+20
         level_indicator :	1
         gds_grid_type :	0
         parameter_table_version :	128
         parameter_number :	151
         forecast_time :	12
         forecast_time_units :	hours
         initial_time :	10/01/2011 (00:00)

      float R_GDS0_ISBL ( lv_ISBL2, g0_lat_0, g0_lon_1 )
         center :	European Center for Medium-Range Weather Forecasts (RSMC)
         long_name :	Relative humidity
         units :	%
         _FillValue :	1e+20
         level_indicator :	100
         gds_grid_type :	0
         parameter_table_version :	128
         parameter_number :	157
         forecast_time :	12
         forecast_time_units :	hours
         initial_time :	10/01/2011 (00:00)

      float BLH_GDS0_SFC ( g0_lat_0, g0_lon_1 )
         center :	European Center for Medium-Range Weather Forecasts (RSMC)
         long_name :	Boundary layer height
         units :	m
         _FillValue :	1e+20
         level_indicator :	1
         gds_grid_type :	0
         parameter_table_version :	128
         parameter_number :	159
         forecast_time :	12
         forecast_time_units :	hours
         initial_time :	10/01/2011 (00:00)

      float 10U_GDS0_SFC ( g0_lat_0, g0_lon_1 )
         center :	European Center for Medium-Range Weather Forecasts (RSMC)
         long_name :	10 metre U wind component
         units :	m s**-1
         _FillValue :	1e+20
         level_indicator :	1
         gds_grid_type :	0
         parameter_table_version :	128
         parameter_number :	165
         forecast_time :	12
         forecast_time_units :	hours
         initial_time :	10/01/2011 (00:00)

      float 10V_GDS0_SFC ( g0_lat_0, g0_lon_1 )
         center :	European Center for Medium-Range Weather Forecasts (RSMC)
         long_name :	10 metre V wind component
         units :	m s**-1
         _FillValue :	1e+20
         level_indicator :	1
         gds_grid_type :	0
         parameter_table_version :	128
         parameter_number :	166
         forecast_time :	12
         forecast_time_units :	hours
         initial_time :	10/01/2011 (00:00)

      float 2T_GDS0_SFC ( g0_lat_0, g0_lon_1 )
         center :	European Center for Medium-Range Weather Forecasts (RSMC)
         long_name :	2 metre temperature
         units :	K
         _FillValue :	1e+20
         level_indicator :	1
         gds_grid_type :	0
         parameter_table_version :	128
         parameter_number :	167
         forecast_time :	12
         forecast_time_units :	hours
         initial_time :	10/01/2011 (00:00)

      float 2D_GDS0_SFC ( g0_lat_0, g0_lon_1 )
         center :	European Center for Medium-Range Weather Forecasts (RSMC)
         long_name :	2 metre dewpoint temperature
         units :	K
         _FillValue :	1e+20
         level_indicator :	1
         gds_grid_type :	0
         parameter_table_version :	128
         parameter_number :	168
         forecast_time :	12
         forecast_time_units :	hours
         initial_time :	10/01/2011 (00:00)

      float SSR_GDS0_SFC ( g0_lat_0, g0_lon_1 )
         center :	European Center for Medium-Range Weather Forecasts (RSMC)
         long_name :	Surface solar radiation
         units :	W m**-2 s
         _FillValue :	1e+20
         level_indicator :	1
         gds_grid_type :	0
         parameter_table_version :	128
         parameter_number :	176
         forecast_time :	12
         forecast_time_units :	hours
         initial_time :	10/01/2011 (00:00)

      float STR_GDS0_SFC ( g0_lat_0, g0_lon_1 )
         center :	European Center for Medium-Range Weather Forecasts (RSMC)
         long_name :	Surface thermal radiation
         units :	W m**-2 s
         _FillValue :	1e+20
         level_indicator :	1
         gds_grid_type :	0
         parameter_table_version :	128
         parameter_number :	177
         forecast_time :	12
         forecast_time_units :	hours
         initial_time :	10/01/2011 (00:00)

      float TSR_GDS0_SFC ( g0_lat_0, g0_lon_1 )
         center :	European Center for Medium-Range Weather Forecasts (RSMC)
         long_name :	Top solar radiation
         units :	W m**-2 s
         _FillValue :	1e+20
         level_indicator :	1
         gds_grid_type :	0
         parameter_table_version :	128
         parameter_number :	178
         forecast_time :	12
         forecast_time_units :	hours
         initial_time :	10/01/2011 (00:00)

      float TTR_GDS0_SFC ( g0_lat_0, g0_lon_1 )
         center :	European Center for Medium-Range Weather Forecasts (RSMC)
         long_name :	Top thermal radiation
         units :	W m**-2 s
         _FillValue :	1e+20
         level_indicator :	1
         gds_grid_type :	0
         parameter_table_version :	128
         parameter_number :	179
         forecast_time :	12
         forecast_time_units :	hours
         initial_time :	10/01/2011 (00:00)

      float EWSS_GDS0_SFC ( g0_lat_0, g0_lon_1 )
         center :	European Center for Medium-Range Weather Forecasts (RSMC)
         long_name :	East-West surface stress
         units :	N m**-2 s
         _FillValue :	1e+20
         level_indicator :	1
         gds_grid_type :	0
         parameter_table_version :	128
         parameter_number :	180
         forecast_time :	12
         forecast_time_units :	hours
         initial_time :	10/01/2011 (00:00)

      float NSSS_GDS0_SFC ( g0_lat_0, g0_lon_1 )
         center :	European Center for Medium-Range Weather Forecasts (RSMC)
         long_name :	North-South surface stress
         units :	N m**-2 s
         _FillValue :	1e+20
         level_indicator :	1
         gds_grid_type :	0
         parameter_table_version :	128
         parameter_number :	181
         forecast_time :	12
         forecast_time_units :	hours
         initial_time :	10/01/2011 (00:00)

      float E_GDS0_SFC ( g0_lat_0, g0_lon_1 )
         center :	European Center for Medium-Range Weather Forecasts (RSMC)
         long_name :	Evaporation
         units :	m of water
         _FillValue :	1e+20
         level_indicator :	1
         gds_grid_type :	0
         parameter_table_version :	128
         parameter_number :	182
         forecast_time :	12
         forecast_time_units :	hours
         initial_time :	10/01/2011 (00:00)

      float LCC_GDS0_SFC ( g0_lat_0, g0_lon_1 )
         center :	European Center for Medium-Range Weather Forecasts (RSMC)
         long_name :	Low cloud cover
         units :	(0 - 1)
         _FillValue :	1e+20
         level_indicator :	1
         gds_grid_type :	0
         parameter_table_version :	128
         parameter_number :	186
         forecast_time :	12
         forecast_time_units :	hours
         initial_time :	10/01/2011 (00:00)

      float HCC_GDS0_SFC ( g0_lat_0, g0_lon_1 )
         center :	European Center for Medium-Range Weather Forecasts (RSMC)
         long_name :	High cloud cover
         units :	(0 - 1)
         _FillValue :	1e+20
         level_indicator :	1
         gds_grid_type :	0
         parameter_table_version :	128
         parameter_number :	188
         forecast_time :	12
         forecast_time_units :	hours
         initial_time :	10/01/2011 (00:00)

      float SKT_GDS0_SFC ( g0_lat_0, g0_lon_1 )
         center :	European Center for Medium-Range Weather Forecasts (RSMC)
         long_name :	Skin temperature
         units :	K
         _FillValue :	1e+20
         level_indicator :	1
         gds_grid_type :	0
         parameter_table_version :	128
         parameter_number :	235
         forecast_time :	12
         forecast_time_units :	hours
         initial_time :	10/01/2011 (00:00)

      integer lv_ISBL2 ( lv_ISBL2 )
         long_name :	isobaric level
         units :	hPa

      float g0_lat_0 ( g0_lat_0 )
         long_name :	latitude
         GridType :	Cylindrical Equidistant Projection Grid
         units :	degrees_north
         Dj :	0.25
         Di :	0.25
         Lo2 :	179.75
         La2 :	-90
         Lo1 :	180
         La1 :	90

      float g0_lon_1 ( g0_lon_1 )
         long_name :	longitude
         GridType :	Cylindrical Equidistant Projection Grid
         units :	degrees_east
         Dj :	0.25
         Di :	0.25
         Lo2 :	179.75
         La2 :	-90
         Lo1 :	180
         La1 :	90

 
